package org.mik.first;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mik.first.Services.CompanyService;
import org.mik.first.Services.PersonService;
import org.mik.first.domain.Client;
import org.mik.first.domain.Company;
import org.mik.first.domain.Person;

import java.util.ArrayList;
import java.util.List;

public class Control {
    private final static Logger LOG=  LogManager.getLogger();
    private final static boolean DEBUG_TEMPORARY = true;

    private final PersonService personService;
    private final CompanyService companyService;

    private long id;

    public Control() {
        this.personService = new PersonService();
        this.companyService = new CompanyService();
        this.id =0;
    }
    public  void start()
    {
        List<String[]> dummyList = createDummyList();
        Client client;
        for (String[] strings : dummyList){

            client = this.converterFromString(strings);
            this.dewIt(client);
    }

    }
    private Client converterFromString(String[] strings){

        switch (strings[0]){
            case "p" : return new Person(id++, strings[1], strings[2],strings[3]);
            case "c" : return new Company(id++, strings[1], strings[2],strings[3]);
            default:if (DEBUG_TEMPORARY)
                LOG.debug("Control.converterFromString error: unknown flag" + strings[0]);
            throw new RuntimeException("Control.converterFromString error: unknown flag" + strings[0]);
        }
    }

    private void dewIt(Client client){

        if (client instanceof Person)
        {
            this.personService.pay((Person)client);
            return;
        }
        if (client instanceof Company){
            this.companyService.pay((Company)client);
            return;
        }
        throw new RuntimeException("Unknown Client");
    }

    private List<String[]> createDummyList(){

        List<String[]>  dummyList = new ArrayList<>();
        dummyList.add(new String[] { "p", "Zaphod Beeblebrox" , "Boszorkany", "985622458"});
        dummyList.add(new String[] { "p", "Linux" , "Ferenc", "42254698"});
        dummyList.add(new String[] { "p", "Windows" , "Kiraly utca", "9657852"});
        dummyList.add(new String[] { "p", "Ferencesek" , "garay utca", "2568974"});
        dummyList.add(new String[] { "c", "Google" , "USA", "852365P"});
        dummyList.add(new String[] { "c", "Oracle" , "USA", "75632145Y"});
        dummyList.add(new String[] { "c", "Microsoft" , "USA", "362547T"});

        return  dummyList;
    }


}





















