package org.mik.first.Services;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mik.first.domain.Company;
import org.mik.first.domain.Person;

public class PersonService implements Service<Person>{
    private final static Logger LOG=  LogManager.getLogger();
    private final static boolean DEBUG_TEMPORARY = true;



    @Override
    public void pay(Person client) {
        if (DEBUG_TEMPORARY)
            LOG.debug( "Enter PersonService" + client);
        System.out.println("Enter PersonService" + client);

    }

    @Override
    public void receiveService(Person client) {

            LOG.info("Enter PersonService.receiveService" + client);
        System.out.println("Enter PersonService" + client);
    }
}
