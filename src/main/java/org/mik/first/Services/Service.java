package org.mik.first.Services;

import org.mik.first.domain.Client;

public interface Service <T extends Client>{
    public void pay(T Client);

    public void receiveService(T Client);

}
