package org.mik.first.domain;

public class Person extends Client{

    private String idNumber;

    public Person (){
        super();
    }

    public Person(String idNumber) {

        this.idNumber = idNumber;
    }

    public Person(Long id, String name, String address, String idNumber) {
        super(id, name, address);
        this.idNumber = idNumber;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Person)) return false;
        if (!super.equals(o)) return false;

        Person person = (Person) o;

        return idNumber != null ? idNumber.equals(person.idNumber) : person.idNumber == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (idNumber != null ? idNumber.hashCode() : 0);
        return result;
    }

    public String toString() {
        return "Person{" +
                "idNumber='" + idNumber + '\'' +
                super.toString()+
                '}';
    }
}
