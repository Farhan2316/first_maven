package org.mik.first.domain;

public class Company extends Client {
    private String taxNumber;

    public Company(){

    }

    public Company(String taxNumber) {

        this.taxNumber = taxNumber;
    }

    public Company(Long id, String name, String address, String taxNumber) {
        super(id, name, address);
        this.taxNumber = taxNumber;
    }

    public String getTaxNumber() {
        return taxNumber;
    }

    public void setTaxNumber(String taxNumber) {
        this.taxNumber = taxNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Company)) return false;
        if (!super.equals(o)) return false;

        Company company = (Company) o;

        return taxNumber != null ? taxNumber.equals(company.taxNumber) : company.taxNumber == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (taxNumber != null ? taxNumber.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Company{" +
                "taxNumber='" + taxNumber + '\'' +
                super.toString()+
                '}';
    }
}
